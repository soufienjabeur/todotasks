## Steps to apply TDD approach (Test Driven Developpement)

**TDD:** a paradigm where you write a test case and do minimalistic changes to the code to make it pass along with the
existing test cases.

- Quickly add a test
- Run all the tests and see the new one fail
- Make minimalistic changes to the code
- Run all tests and see them all succeed
- Refactor to remove duplication

In this project, we will develop a Rest API following TDD approach

**REST**

![Layered Architectureyer](src/main/resources/image/REST_layer.PNG)

**Controller**

we start with the first layer in test:

- we create ToDoControllerTest.java inside the test folder
- To register this class as spring test class which is testing a MVC controller, we have to mark this class with two
  annotations **Extendwith** and **WebMVCTest**
- We need a mockMvc object, so we create new one and we annote it with **Autowired**, because Spring has an
  ApplicationContext where it stores objects of the classes which are marked with annotations like **Service**,
  **Component**, etc...
- In unit testing, we want **each module to be tested independently**. But our Controller depends on the Service layer.
  Since we are only focused on the Controller code, it is natural **to mock the Service layer code** using **MockBean**
  annotation for our unit tests.
- We can't compile this code as ToDoService doesn't exist, so we should add it and annote it with **Service**
  annotation.
- Now we write test for function getAllToDos()
- We can't compile our code,because we should add ToDo entity, so we add it and we annote it with **Entity** anntation
- We have another issue, our findAll function doesn't exist in the ToDoService so we should add it
- Anothe test fail, because the endpoint that we try to hit with get method doesn't exist yet, so we should add
  ToDoControl and annote it with **RestController** and we add this endpoint.
- It's passing

**Service**

- we create ToDoServiceTest which is our SUB(Subject Under Test) and we have to mock all the dependencies of the SUT, in
  our case is the repository and we annote it with **MockBean** annotation. Also here, we don't want to mockMVC as we
  are not dealing with controllers. Mark the class with **SpringBootTest** annotation which provided by Spring Boot to
  test it's components. This annotation adds **
  ExtendWith** annotation implicitly.
- We shoudl add ToDoRepository to our porject and annote it with **Repository**
- We add test to getAllToDos()